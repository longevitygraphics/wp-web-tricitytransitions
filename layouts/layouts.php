<?php

switch ( get_row_layout() ) {
	case 'how_we_can_help':
		get_template_part( '/layouts/layouts/how-we-can-help' );
		break;
	case 'our_board_of_directors':
		get_template_part( '/layouts/layouts/our_board_of_directors' );
		break;
	case 'about__tabs':
		get_template_part( '/layouts/layouts/about__tabs' );
		break;
	case 'donate_tabs':
		get_template_part( '/layouts/layouts/donate_tabs' );
		break;
	case 'gallery':
		get_template_part( '/layouts/layouts/gallery' );
		break;
}
