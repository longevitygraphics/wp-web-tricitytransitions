<?php
/**
 * Text Block Layout
 *
 */
?>

<?php

get_template_part( '/layouts/partials/block-settings-start' );
$tab_counter = 0;
?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<div class="d-flex flexible_text <?php if ( $container == 'container-wide' ) {
	echo 'no-gutters';
} ?> row <?php the_sub_field( 'align_items_vertical' ); ?> <?php the_sub_field( 'align_items_horizontal' ); ?>">
	<div class="col-12">
		<?php if ( have_rows( 'about_tabs_items' ) ) : ?>
			<ul class="nav about-tabs d-none d-md-flex" id="aboutTabs" role="tablist">

				<?php while ( have_rows( 'about_tabs_items' ) ) : the_row();
					$tab_counter ++; ?>
					<li class="nav-item">
						<a class="nav-link <?php if ( $tab_counter == 1 ) {
							echo 'active';
						} ?>" id="about-tab-<?php echo $tab_counter; ?>" data-toggle="tab"
						   href="#about-<?php echo $tab_counter; ?>" role="tab"
						   aria-controls="about-<?php echo $tab_counter; ?>" aria-selected="false">
							<?php echo get_sub_field( 'title' ) ?>
						</a>
					</li>
				<?php endwhile; ?>
			</ul>
			<div class="tab-content about-tabs-content" id="aboutTabsContent">
				<?php $tab_counter = 0;
				while ( have_rows( 'about_tabs_items' ) ) : the_row();
					$tab_counter ++; ?>
					<div class="tab-pane fade show <?php if ( $tab_counter == 1 ) {
						echo 'active';
					} ?>"
					     id="about-<?php echo $tab_counter; ?>"
					     role="tabpanel"
					     aria-labelledby="about-tab-<?php echo $tab_counter; ?>">
						<h2 class="d-md-none text-center">
							<span class="fancy-title"><?php echo get_sub_field( 'title' ) ?></span>
						</h2>
						<div class="row">
							<div class="col-md-6">
								<?php echo get_sub_field( 'content' ) ?>
							</div>
							<div class="col-md-6">
								<?php $tab_image = get_sub_field( 'image' ); ?>
								<img src="<?php echo $tab_image['sizes']['medium_large']; ?>"
								     alt="<?php echo $tab_image['alt']; ?>"
								     title="<?php echo $tab_image['title']; ?>"
								/>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>
	</div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

get_template_part( '/layouts/partials/block-settings-end' );

?>
