<?php
/**
 * Text Block Layout
 *
 */
?>

<?php

get_template_part( '/layouts/partials/block-settings-start' );

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<div class="d-flex flexible_text <?php if ( $container == 'container-wide' ) {
	echo 'no-gutters';
} ?> row <?php the_sub_field( 'align_items_vertical' ); ?> <?php the_sub_field( 'align_items_horizontal' ); ?>">
	<div class="col-12">
		<?php if ( have_rows( "how_we_can_help_links" ) ) : ?>
			<div class="how-can-we-help">
				<div class="row">
					<?php while ( have_rows( "how_we_can_help_links" ) ) : the_row();
						$how_link = get_sub_field( 'link' );
						?>
						<a href="<?php echo $how_link['url']; ?>" title="<?php echo $how_link['title']; ?>">
							<?php echo file_get_contents( get_stylesheet_directory_uri() . '/assets/dist/images/' . get_sub_field( 'icon' ) ); ?>
							<span><?php echo $how_link['title']; ?></span>
						</a>
					<?php endwhile; ?>
				</div>
			</div>
		<?php endif; ?>

	</div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

get_template_part( '/layouts/partials/block-settings-end' );

?>
