<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<div class="gallery-layout">
				<?php  
					$gallery_title = get_sub_field('gallery_title');
				?>
				<div class="gallery-title mb-3">
					<?php echo $gallery_title; ?>
				</div>
				<?php if (have_rows("gallery_images")): ?>
					<div class="gallery d-flex flex-wrap">
						<?php while (have_rows("gallery_images")): the_row(); ?>
							<?php 
								$gallery_image = get_sub_field("image");
							 ?>
							<a class="col-sm-6 col-md-4 col-lg-3 mb-3" href="<?php echo $gallery_image['url']; ?>" data-lightbox="roadtrip"><img src="<?php echo $gallery_image['url']; ?>" alt="<?php echo $gallery_image['alt']; ?>"></a>
						<?php endwhile ?>
					</div>
				<?php endif ?>
				
			</div>
		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
