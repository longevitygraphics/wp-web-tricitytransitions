<?php
/**
 * Text Block Layout
 *
 */
?>

<?php
get_template_part( '/layouts/partials/block-settings-start' );
?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<div class="d-flex flexible_text <?php if ( $container == 'container-wide' ) {
	echo 'no-gutters';
} ?> row <?php the_sub_field( 'align_items_vertical' ); ?> <?php the_sub_field( 'align_items_horizontal' ); ?>">
	<div class="col-12">
		<div class="directors">
			<div class="directors__content">
				<?php echo get_sub_field( 'our_board_of_directors_content' ) ?>
			</div>


			<div class="directors__items row justify-content-center">
				<?php
				foreach ( get_sub_field( 'our_board_of_directors_list' ) as $director ) :?>
					<div class="director__item col-sm-6 col-md-4">
						<a href="<?php echo get_permalink($director); ?>"><img src="<?php echo get_the_post_thumbnail_url( $director, 'medium' ) ?>"
						     alt="<?php echo get_the_title( $director ) ?>">
						</a>
						<h3><?php echo get_the_title( $director ) ?></h3>
						<p><?php echo get_field( 'occupation', $director ) ?></p>
					</div>
				<?php endforeach; ?>
			</div>
		</div>

	</div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

get_template_part( '/layouts/partials/block-settings-end' );

?>
