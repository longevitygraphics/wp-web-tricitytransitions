<?php
/**
 * Text Block Layout
 *
 */
?>

<?php

get_template_part( '/layouts/partials/block-settings-start' );
$tab_counter = 0;
?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<div class="d-flex flexible_text <?php if ( $container == 'container-wide' ) {
	echo 'no-gutters';
} ?> row <?php the_sub_field( 'align_items_vertical' ); ?> <?php the_sub_field( 'align_items_horizontal' ); ?>">
	<div class="col-12">
		<?php if ( have_rows( 'donate_tabs_items' ) ) : ?>
			<ul class="nav donate-tabs d-none d-md-flex" id="donateTabs" role="tablist">

				<?php while ( have_rows( 'donate_tabs_items' ) ) : the_row();
					$tab_counter ++; ?>
					<li class="nav-item">
						<a class="nav-link <?php if ( $tab_counter == 1 ) {
							echo 'active';
						} ?>" id="donate-tab-<?php echo $tab_counter; ?>" data-toggle="tab"
						   href="#donate-<?php echo $tab_counter; ?>" role="tab"
						   aria-controls="donate-<?php echo $tab_counter; ?>" aria-selected="false">
							<?php echo file_get_contents( get_stylesheet_directory_uri() . '/assets/dist/images/' . get_sub_field( 'icon' ) ); ?>
							<?php echo get_sub_field( 'title' ) ?>
						</a>
					</li>
				<?php endwhile; ?>
			</ul>
			<div class="tab-content donate-tabs-content" id="donateTabsContent">
				<?php $tab_counter = 0;
				while ( have_rows( 'donate_tabs_items' ) ) : the_row();
					$tab_counter ++; ?>
					<div class="tab-pane fade show <?php if ( $tab_counter == 1 ) {
						echo 'active';
					} ?>"
					     id="donate-<?php echo $tab_counter; ?>"
					     role="tabpanel"
					     aria-labelledby="donate-tab-<?php echo $tab_counter; ?>">
						<h2 class="d-md-none text-center tab-title bg-lightbg">
							<span class="fancy-title"><?php echo get_sub_field( 'title' ) ?></span>
						</h2>
						<div class="content">
							<?php echo get_sub_field( 'content' ) ?>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>
	</div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

get_template_part( '/layouts/partials/block-settings-end' );

?>
