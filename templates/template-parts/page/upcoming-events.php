<?php
$current_date          = date( 'Y-m-d' ) . ' 00:00:00';
$upcoming_events_args  = array(
	'post_type'  => 'tribe_events',
	'count'      => 1,
	'meta_query' => array(
		array(
			'key'     => '_EventStartDate',
			'value'   => $current_date,
			'compare' => '>'
		),
	),
	'orderby' => 'meta_value'
);
$upcoming_events_query = new WP_Query( $upcoming_events_args );
?>
<?php if ( $upcoming_events_query->have_posts() ) : ?>

	<div class="upcoming-events">
		<?php while ( $upcoming_events_query->have_posts() ) : $upcoming_events_query->the_post();
			$event_id         = get_the_ID();
			$event_start_date = get_post_meta( $event_id, '_EventStartDate', true );
			$event_end_date   = get_post_meta( $event_id, '_EventEndDate', true );
			$event_venue_id   = get_post_meta( $event_id, '_EventVenueID', true );
			$event_venue_name = '';
			if ( $event_venue_id ) {
				$event_venue_name = get_the_title( $event_venue_id );
			}
			?>
			<div class="upcoming-event--item">
				<h4><?php echo the_title() ?></h4>
				<?php if ( $event_start_date ) : ?>
					<p><?php echo date( 'F jS', strtotime( $event_start_date ) ); ?></p>
					<p><?php echo date( 'g:i A', strtotime( $event_start_date ) ); ?></p>
				<?php endif; ?>
				<?php echo ( $event_venue_name ) ? $event_venue_name : '' ?>

			</div>
		<?php endwhile; ?>
	</div>
<?php endif; ?>
<?php wp_reset_postdata(); ?>
