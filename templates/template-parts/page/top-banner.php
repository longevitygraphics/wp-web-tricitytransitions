<?php
$top_banner_bg    = get_field( 'top_banner_bg' );
$top_banner_text  = get_field( 'top_banner_text' );
$top_banner_style = '';
$top_banner_class = 'bg-aqua';
$events_top_banner = get_field( 'events_default_banner','options' );
if ( $top_banner_bg and $top_banner_bg['url'] ) {
	$top_banner_style = 'background-image: url(' . $top_banner_bg['url'] . ')';
	$top_banner_class = 'top-banner--bg';
}
?>
<?php if ( $top_banner_text ) : ?>
    <div class="top-banner <?php echo $top_banner_class ?>" style="<?php echo $top_banner_style ?>">
        <div class="container">
            <div class="top-banner__content">
				<?php echo $top_banner_text ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<!-- If it's events page -->
<?php if (is_archive('events') && $events_top_banner && !is_single()): ?>
	<div class="top-banner top-banner--bg" style="background-image: url('<?php echo $events_top_banner["url"]; ?>')">
		<div class="container">
            <div class="top-banner__content">
				<h1 class="text-white">Events</h1>
            </div>
        </div>
		
	</div>
<?php endif ?>
<!-- End of if it's events page -->

<!-- If it's events single page -->
<?php if (tribe_is_event() && is_single() && $events_top_banner): ?>
	<div class="top-banner top-banner--bg" style="background-image: url('<?php echo $events_top_banner["url"]; ?>')">
		<div class="container">
            <div class="top-banner__content">
				<h1 class="text-white"><?php echo tribe_get_events_title(); ?></h1>
            </div>
        </div>
		
	</div>
<?php endif ?>
<!-- End of if it's events single page -->