<?php
// get latest 3 blog posts
$args      = array(
	'numberposts' => 3,
	'order'       => 'DESC',
	'orderby'     => 'date',
	'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => 'featured'
        )
    )
);
$postslist = get_posts( $args );

// pr($postslist);
?>
<div class="whats-new">
	<div class="whats-new-items">
		<div class="d-flex flex-wrap justify-content-center">
			<?php if ( $postslist ) : ?>
				<?php foreach ( $postslist as $item ) : ?>
					<div class="col-4">
						<div class="whats-new-item">
							<div class="whats-new-item__img"
								 style="background-image: url(<?php echo get_the_post_thumbnail_url( $item ); ?>)"></div>
							<div class="whats-new-item__content">
								<h3><?php echo get_the_title( $item ); ?></h3>
								<p><?php echo get_the_excerpt( $item ); ?></p>
								<a href="<?php echo $item->guid; ?>">Read More</a>
							</div>
						</div>
					</div>
				<?php endforeach; ?>

			<?php endif; ?>
		</div>
	</div>
</div>
