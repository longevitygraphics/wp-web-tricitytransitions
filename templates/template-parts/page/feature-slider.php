<?php
?>
<?php if ( have_rows( 'top_slides' ) ) : ?>
	<div class="top-slider">
		<?php while ( have_rows( 'top_slides' ) ) : the_row();
			$slide_img = get_sub_field( 'image' );
			?>
			<div class="top-slider--item">
				<img class="top-slider--img" src="<?php echo $slide_img['url'] ?>" alt="<?php echo $slide_img['alt'] ?>">
				<div class="top-slider--content">
					<div class="container">
						<div class="top-slider--inner">
							<?php echo get_sub_field( 'content' ) ?>
						</div>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
<?php endif; ?>
