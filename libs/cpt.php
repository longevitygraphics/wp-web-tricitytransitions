<?php

function lg_custom_post_type(){
    register_post_type( 'director',
        array(
            'labels' => array(
                'name' => __( 'Directors' ),
                'singular_name' => __( 'Directors' )
            ),
            'public' => true,
            'rewrite' => array(
                'with_front' => false
            ),
            'has_archive' => false,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'lg_menu',
            'supports' => array( 'thumbnail','title' )
        )
    );
}

add_action( 'init', 'lg_custom_post_type' );