<?php

global $lg_tinymce_custom;
$selector          = 'h1,h2,h3,h4,h5,h6';
$lg_tinymce_custom = array(
	'title' => 'Custom',
	'items' => array(
		array(
			'title'    => 'Fancy Title',
			'inline' => 'span',
			'classes'  => 'fancy-title',
		),
		array(
			'title'    => 'Scroll Link',
			'inline' => 'a',
			'classes'  => 'scroll-link',
		)
	)
);
