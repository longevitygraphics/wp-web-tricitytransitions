<?php
function nav_items( $items, $args ) {
	lg_write_log( $args );
	if ( $args->menu->slug == 'main-menu' ) {
		$items .= '<li class="d-none d-lg-block menu-item menu-item-type-custom nav-item"><a href="/donations/" class="nav-link btn btn-heart">Donate</a></li>';
	}

	return $items;
}

add_filter( 'wp_nav_menu_items', 'nav_items', 10, 2 );

function featured_banner_top() {
	ob_start(); ?>
	<?php
	get_template_part( '/templates/template-parts/page/top-banner' );
	get_template_part( '/templates/template-parts/page/feature-slider' );
	?>
	<?php echo ob_get_clean();
}

add_action( 'wp_content_top', 'featured_banner_top' );

// add widgets
function add_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Delta', '_s' ),
			'id'            => 'footer-delta',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Echo', '_s' ),
			'id'            => 'footer-echo',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

}
add_action( 'widgets_init', 'add_widgets_init' );


//add upcoming event shortcode
add_shortcode("upcoming-events", "lg_upcoming_events");
function lg_upcoming_events($attr){
	ob_start();
	get_template_part( '/templates/template-parts/page/upcoming-events' );
	return ob_get_clean();
}

//pagination
function lg_numeric_posts_nav() {
 
    if( is_singular() )
        return;
 
    global $wp_query;
 
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<div class="navigation"><ul>' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
 
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";
 
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );
 
    echo '</ul></div>' . "\n";
 
}

// Reinstate the 15 minutes incement for the events calendar

add_filter( 'tribe_events_meta_box_timepicker_step', 'tribe_example_fifteen_minute_timepicker' );

function tribe_example_fifteen_minute_timepicker() {
    return 15;
}
