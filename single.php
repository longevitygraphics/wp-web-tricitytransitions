<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

<?php

	$banner_height = get_option('lg_option_blog_single_banner_height') ? get_option('lg_option_blog_single_banner_height') : '400px';

?>
	<main>
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<div class="blog-banner" style="height: <?php echo $banner_height; ?>">
				<?php
					$banner = get_field('blog_default_banner', 'option');
				?>
				<img src="<?php echo $banner['url']; ?>" alt="<?php echo $banner['alt']; ?>">
				<div class="overlay text-center text-white flex-column">
					<h1 class="text-white"><?php the_title(); ?></h1>
					<div class="text-white"><h2 class="h4"><?php echo do_shortcode('[wpseo_breadcrumb]'); ?></h2></div>
				</div>
			</div>

			<div class="container py-5">
				<?php the_content(); ?>
			</div>

		<?php endwhile; endif; ?>

	</main>

<?php get_footer(); ?>