// Windows Ready Handler

(function ($) {
  $(document).ready(function () {
    // supporting partner slider
    $(".supporting-partners-wrapper").slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 10000,
      arrows: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            adaptiveHeight: true,
          },
        },
      ],
    });

    // $('.supporting-partners-wrapper').on('afterChange', function(event, slick, currentSlide, nextSlide) {
    //        $('.supporting-partners-wrapper').slick('refresh');
    // });

    $(".top-slider").slick({
      arrows: true,
      dots: false,
      fade: true,
      autoplay: true,
      autoplaySpeed: 5000,
      responsive: [
        {
          breakpoint: 576,
          settings: {
            arrows: false,
          },
        },
      ],
    });

    $("a.scroll-link").click(function (e) {
      e.preventDefault();
      $("html, body").animate(
        { scrollTop: $($(this).attr("href")).offset().top },
        800
      );
    });
  });
})(jQuery);
