<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php do_action('wp_content_bottom'); ?>
	</div>
	
	<?php do_action('wp_body_end'); ?>
	<?php $lg_option_footer_site_legal = get_option('lg_option_footer_site_legal'); ?>
		<?php if( !get_field('hide_footer_cta') ) : ?>
		<div class="footer-cta py-3 px-3 px-lg-5 d-md-flex align-items-center justify-content-between  bg-primary">
			<?php  
				$footer_cta_text = get_field('footer_cta_text', 'option');
				$footer_cta_button = get_field('footer_cta_button', 'option');
				if ($footer_cta_button && is_array( $footer_cta_button ) && array_key_exists( 'url', $footer_cta_button )) {
					$footer_cta_button_url = $footer_cta_button['url'];
					$footer_cta_button_title = $footer_cta_button['title'];
					$footer_cta_button_target = $footer_cta_button['target'] ? $footer_cta_button['target'] : '_self';
				}
			?>
			<div class="footer-cta-text mb-3 mb-md-0">
				<?php echo $footer_cta_text; ?>
			</div>
			<div class="footer-cta-button">
				<a class="btn btn-purple-dark" href="<?php echo esc_url( $footer_cta_button_url ); ?>" target="<?php echo esc_attr( $footer_cta_button_target ); ?>"><?php echo esc_html( $footer_cta_button_title ); ?></a>
			</div>
		</div>
		<?php endif; ?> 
		<!--end of footer cta-->
		
	</div> <!-- end of above footer -->

	<footer id="site-footer">
		
		<div id="site-footer-main" >
			<div class="clearfix container py-4 px-3 px-lg-5 container-fluid justify-content-center align-items-start flex-wrap">
				<div class="row">
					<div class="site-footer-alpha col-md-6 col-lg-4">
						<?php dynamic_sidebar('footer-alpha'); ?>
					</div>
					<div class="site-footer-bravo col-md-6 col-lg-3">
						<?php dynamic_sidebar('footer-bravo'); ?>
					</div>
					<div class="site-footer-charlie col-md-6 col-lg-2">
						<?php dynamic_sidebar('footer-charlie'); ?>
					</div>
					<div class="site-footer-delta col-md-6 col-lg-3">
						<?php dynamic_sidebar('footer-delta'); ?>
					</div>
					
				</div>
			</div>

			<?php if(!$lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable'): ?>
				<div id="site-legal" class="py-3 px-3">
					<div class="d-flex justify-content-center justify-content-md-between align-items-center flex-wrap">
						<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
						<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
					</div>
				</div>
			<?php endif; ?>
		</div>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>

<?php do_action('document_end'); ?>
