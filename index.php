<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

	<?php
		$banner_height = get_option('lg_option_blog_archive_banner_height') ? get_option('lg_option_blog_archive_banner_height') : '400px';
		$blog_style = get_option('lg_option_blog_style') ? get_option('lg_option_blog_style') : 'list';
	?>

	<main class="blog">
		<div class="blog-banner" style="height: <?php echo $banner_height; ?>">
				<?php
					$banner = get_field('blog_default_banner', 'option');
				?>
				<img src="<?php echo $banner['url']; ?>" alt="<?php echo $banner['alt']; ?>">
				<div class="overlay text-center text-white flex-column">
					<h1 class="text-white">Blog</h1>
					<div class="text-white><h2 class="h4"><?php echo do_shortcode('[wpseo_breadcrumb]'); ?></h2></div>
				</div>
			</div>

		<div class="container">
			<?php
				switch ($blog_style) {
				    case "list":
				        get_template_part( 'templates/template-parts/blog/list');
				        break;
				    case "grid":
				        get_template_part( 'templates/template-parts/blog/grid');
				        break;
				    case "masonary":
				        get_template_part( 'templates/template-parts/blog/masonary');
				        break;
				}
			?>
			<?php lg_numeric_posts_nav(); ?>
		</div>
	</main>

<?php get_footer(); ?>